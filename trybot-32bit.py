#!/usr/bin/env python

import os
import sys
import subprocess
import threading
import trybot
import requests
import check

sys.stdout.reconfigure(line_buffering=True)

iter = 0
homedir = trybot.trybot_dir + "32bit/"
webdir = trybot.webbase + "32bit/"
weburl = trybot.webbaseurl + "32bit/"

os.chdir (homedir)

# Normally true, but for testing...
baseline_is_dirty = False

# Returns (bool,str) = bool is true if we "passed", if it's false, STR
# has extra text to include in the logs.
def check_regressions (oldfn, newfn):
    global baseline
    baseline = {}
    ret_str = ""
    ret_bool = True

    old = open (oldfn)
    while True:
        line = old.readline()
        if not line:
            break
        (res, name) = line.split(': ', 2)
        name = name.strip()
        #print("OLD: res<"+res+"> name<"+name+">")
        baseline[name] = res
    old.close()

    new = open (newfn)
    while True:
        line = new.readline()
        if not line:
            break
        (res, name) = line.split(': ', 2)
        name = name.strip()
        if name in baseline:
            o = baseline[name]
        else:
            o = 'NONE'
        if res != o and res == "FAIL":
            ret_bool = False
            ret_str = ret_str + o + " -> " + res + " : " + name + "\n"
            #print("NEW: res<"+res+"> name<"+name+"> -> "+o)
    new.close()

    return (ret_bool, ret_str)

def pass_ok (p):
    pf = open (T + "/" + p + ".status")
    pd = pf.read ()
    pf.close ()
    if "0" in pd:
        return True
    return False

def save_results (T, d, id, PID):
    global baseline

    if not os.path.isdir (webdir + str(id)):
        os.mkdir (webdir + str(id))
    for fn in ["the-patch", "apply.status", "apply.log",
               "make.status", "make.log", "make.tail",
               "check.status", "check.sum", "regressions"]:
        if os.path.isfile (T+'/'+fn):
            f = str(id)+"/"+fn+".txt"
            trybot.run ("cp", T+"/"+fn, webdir+f);

    if os.path.isfile (T + "/check.sum"):
        sum = open (T + "/check.sum")
        while True:
            line = sum.readline()
            if not line:
                break
            (res, name) = line.split(': ', 2)
            name = name.strip()
            if name in baseline:
                o = baseline[name]
            else:
                o = 'NONE'
            if res != o and res == "FAIL":
                name2 = name.replace ("/", "-")
                trybot.run ("podman", "cp", "trybot-rv32-"+PID+":/build/"+name+".out", webdir+str(id)+"/"+name2+".out")
        sum.close()

    check.check_setup (description = d, url = weburl + str(id) + "/")
    check.check_fail ()

def cleanup (PID):
    global T;
    trybot.run ("podman", "rm", "trybot-rv32-"+PID);
    trybot.run ("podman", "rmi", "trybot-rv32-"+PID);
    trybot.run ("rm", "-rf", T);

def regenerate_baseline ():
    global baseline_is_dirty
    baseline_is_dirty = False
    print ("\nTrybot is regenerating the baseline")
    trybot.run (trybot.rebuild_script)

def builder_cb(event):
    global T
    global iter
    global baseline_is_dirty

    if event['type'] == 'COMMIT':
        print ("\nTrybot noted that a commit happened")
        baseline_is_dirty = True
        return
    if baseline_is_dirty:
        regenerate_baseline ()

    print ("\nTryBot 32bit was called for patch series", event['series_id'], '!')

    check.check_setup ('TryBot-32bit', 'Build for rv32', None)

    iter = iter + 1
    PID = str(os.getpid()) + "-" + str(iter)
    os.chdir (homedir)

    try:
        resp = requests.get (event['data']['mbox'])
    except:
        print (" - skipping: unable to download patch series mbox");
        return;
    ptext = resp.text

    c1 = ptext.find("committed")
    c2 = ptext.find("COMMITTED")
    if c1 > 0 or c2 > 0:
        print (" - skipping: committed")
        return

    T = "/tmp/trybot."+PID
    if not os.path.isdir (T):
        os.mkdir (T)

    patchfile = open ("trybot-pod/the-patch.txt", "w")
    patchfile.write (ptext)
    patchfile.close ()

    patchtmp = open (T + "/the-patch", "w")
    patchtmp.write (ptext)
    patchtmp.close ()

    trybot.run ("podman", "build", "-t", "trybot-rv32-"+PID, "trybot-pod")

    trybot.run ("podman", "run", "-d=false", "--network=none",
                 "--name", "trybot-rv32-"+PID, "trybot-rv32-"+PID, "/do-build.sh")

    for fn in ["apply.status", "apply.log", "make.status", "make.log", "make.tail", "check.status", "check.sum"]:
        # The copy might fail but we don't care; we know which files are "live" by reading the status files.
        trybot.run ("podman", "cp", "trybot-rv32-"+PID+":/"+fn, T+"/"+fn)

    if not pass_ok ("apply"):
        print("patch failed to apply")
        save_results (T, "Patch series failed to apply", event['data']['id'], PID)
        cleanup (PID)
        return

    if not pass_ok ("make"):
        print("patch failed to build")
        save_results (T, "Patch series failed to build", event['data']['id'], PID)
        cleanup (PID)
        return

    reg, which = check_regressions (trybot.rebuild_check, "/tmp/trybot."+PID+"/check.sum")
    if not reg:
        print("patch caused regressions")

        rf = open(T + "/regressions", "w")
        rf.write (which)
        rf.close ()

        save_results (T, "Patch caused testsuite regressions", event['data']['id'], PID)
        cleanup (PID)
        return

    print("build was successful, no regressions")
    check.check_success()

    cleanup (PID)

trybot.start ('build32bit', builder_cb)
