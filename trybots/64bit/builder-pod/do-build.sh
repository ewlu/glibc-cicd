#!/bin/bash


# Sources are in /glibc
# patch is in /the-patch.txt

# expected output:
# /apply.log
# /apply.status
# /make.log
# /make.tail (if status is nonzero)
# /make.status
# /check.log
# /check.status

set -vx

export TIMEOUTFACTOR=20

cd /glibc

# Patch must exist and have a size greater than zero.  Empty patches
# are used to generate baselines.
if test -s /the-patch.txt
then
    if git apply -p1 < /the-patch.txt > /apply.log 2>&1
    then
	echo 0 > /apply.status
    else
	echo 1 > /apply.status
	exit 1
    fi
fi

test -d /build || mkdir /build
cd /build

if /glibc/configure CC="gcc " CXX="g++ " --prefix=/usr --build=riscv64-unknown-linux-gnu --host=riscv64-unknown-linux-gnu > /make.log 2>&1
then
    echo 0 > /make.status
else
    echo 1 > /make.status
    exit 1
fi

if make -k -O -j`nproc` >> /make.log 2>&1
then
    echo 0 > /make.status
else
    make -k > /make.tail 2>&1
    echo 1 > /make.status
    exit 1
fi

if make -k -O -j`nproc` check > /check.log 2>&1
then
    echo 0 > /check.status
else
    cat */subdir-tests.sum > /check.sum
    echo 1 > /check.status
    exit 1
fi

exit 0
