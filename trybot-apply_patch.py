#!/usr/bin/env python

import os
import sys
import subprocess
import threading
import trybot
import requests
import check

sys.stdout.reconfigure(line_buffering=True)

homedir = trybot.trybot_dir + "apply_patch/"
webdir = trybot.webbase + "apply_patch/"
weburl = trybot.webbaseurl + "apply_patch/"

# Run a support command, fail trybot if it fails.
def runf (*args):
    (rv, out) = trybot.runv (args);
    if rv != 0:
        exit (0)
    return out;

# We write the patch to 'patch' in a separate thread to avoid I/O deadlock
def write_patch_thread (fd, text):
    fd.write (text)
    fd.close ()

def apply_patch_cb(event):
    print ("TryBot Apply_Patch was called for patch series", event['series_id'], '!')

    check.check_setup ('TryBot-apply_patch', None, None);

    os.chdir (homedir)

    # Ensure a clean git tree
    if os.path.isdir(".git"):
        runf ("git", "clean", "-d", "-f");
        runf ("git", "reset", "--hard", "HEAD");
        runf ("git", "checkout", "master");
        runf ("git", "pull", "--rebase");
        runf ("git", "clean", "-d", "-f");
    else:
        runf ("git", "clone", event['data']['project']['scm_url']);
        runf ("git", "config", "advice.detachedHead", "false");

    # Set the git tree to "as of" the time the patch was posted.
    (rv, hash) = trybot.run ("git", "rev-list", "-n", "1", "--before="+event['date']+"Z", "master");
    if rv != 0:
        exit (1);
    hash = hash.strip()
    runf ("git", "checkout", hash);

    # First gather the patches to be tested
    for patch in event['data']['patches']:
        try:
            resp = requests.get (patch['mbox']);
        except:
            print (" - skipping: unable to download patch series mbox");
            return;

        patch['ptext'] = resp.text;

    # See if any of them have already been applied.  We do this first
    # because once we start applying patches, the tree is dirty and
    # the if-applied check becomes unreliable.
    for patch in event['data']['patches']:
        ptext = patch['ptext'];

        # we have to provide stdin and capture stdout simultaneously
        proc = subprocess.Popen (('git', 'apply', '-p1', '--reverse', '--check'),
                                 stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                                 close_fds=True, text=True, shell=False)

        fin = proc.stdin
        wthread = threading.Thread (target=write_patch_thread, args=(fin, ptext))
        wthread.start ()

        fout = proc.stdout
        sout = fout.read ()
        fout.close ()

        rv = proc.wait ()
        wthread.join ()

        print ("rev-rv", rv, "patch", patch['id'], patch['name']);

        if (rv == 0):
            patch['already-committed'] = 1
        else:
            patch['already-committed'] = 0

    # Now apply the patches in series
    for patch in event['data']['patches']:
        ptext = patch['ptext'];

        # we have to provide stdin and capture stdout simultaneously
        proc = subprocess.Popen (('git', 'apply', '-p1'),
                                 stdout=subprocess.PIPE, stdin=subprocess.PIPE,
                                 close_fds=True, text=True, shell=False)

        fin = proc.stdin
        wthread = threading.Thread (target=write_patch_thread, args=(fin, ptext))
        wthread.start ()

        fout = proc.stdout
        sout = fout.read ()
        fout.close ()

        rv = proc.wait ()
        wthread.join ()

        print ("rv", rv, "patch", patch['id'], patch['name']);
        c1 = patch['name'].find("committed")
        c2 = patch['name'].find("COMMITTED")
        if c1 > 0 or c2 > 0:
            print (" - skipping: committed")
            continue

        check.check_set_patch (patch['id'])
        check.check_setup ()

        if rv != 0:

            logfile = open ("/home/web/trybots/apply_patch/"+str(patch['id'])+".txt", "w")
            logfile.write (sout)

            for line in sout.splitlines():
                s = line.find("saving rejects to file")
                if s > 0:
                    s += 23
                    rejfile = open (line[s:], "r");
                    if rejfile:
                        rej = rejfile.read ()
                        rejfile.close()

                    logfile.write("\n");
                    logfile.write("Reject file " + line[s:] + ":\n");
                    logfile.write (rej);

            logfile.close ();

            logfile = open ("/home/web/trybots/apply_patch/"+str(patch['id'])+"-patch.txt", "w")
            logfile.write (ptext)
            logfile.close ();

            if (patch['already-committed'] == 0):
                check.check_setup (description = "Patch failed to apply to master at the time it was sent",
                                   url = "https://www.delorie.com/trybots/apply_patch/"+str(patch['id'])+".txt")
                check.check_fail ();
            else:
                # It's possible that a patch can apply twice, so we
                # don't warn unless the apply actually fails.
                check.check_setup (description = "Patch already apply to master at the time it was sent")
                check.check_warning ();

        else:
            check.check_setup (description = "Patch applied to master at the time it was sent")
            check.check_success ();

    print ("TryBot Apply_Patch sleeping again...")
    print ("")

print ("TryBot Apply_Patch is running!")

trybot.start ('apply_patch', apply_patch_cb);
