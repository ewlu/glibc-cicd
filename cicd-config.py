#!/usr/bin/python3

# This is the CI/CD config file.  All tools use it, so only include
# things that are cross-tool safe.  I.e. ONLY DEFINE VARIABLES HERE.

#------------------------------------------------------------
# Curator configuration

# These two select one patchwork project instance to poll.
patchworkURL = "https://patchwork.sourceware.org"
patchworkProject = "glibc"

# Poll time starts at the minimum and doubles each time no new events
# are reported, until the max poll time is reached.  If a new event is
# reported, the time resets to the minimum.

# Minimum patchwork event poll time, in seconds.
minPWPoll = 60

# Maximum patchwork event poll time, in seconds.
maxPWPoll = 5*60

# Likewise, for git pulls
minGitPoll = 60
maxGitPoll = 5*60

# Likewise, for email processing
minEMailPoll = 60
maxEMailPoll = 60*60

# Credentials for the mariadb database we'll be using.  Yes, the
# password is plain text.  Fix later ;-)
curatorDBUser = "dj"
curatorDBPassword = "somepw"
curatorDBName = "cicddb"

curatorPort = 4440

# This one is also used by the runner
# Events older than this are removed from the database
eventExpireTime = 3*24*60*60
# How often to check for expired events
eventExpirePollTime = 60*60

#------------------------------------------------------------
# Runner configuration

curatorURL = "https://www.delorie.com/cicd/curator.cgi"

runnerDBUser = "dj"
runnerDBPassword = "somepw"
runnerDBName = "runnerdb"

# Minimum curator poll time, in seconds.
minCuratorPoll = 60

# Maximum curator poll time, in seconds.
maxCuratorPoll = 5*60

rabbitHost = "localhost"
rabbitPort = 5672
rabbitQueue = "cicd"
# actual rabbit queue determined by site

#------------------------------------------------------------
# TryBot configuration

rabbitHost = "localhost"
rabbitPort = 5672
# This varies by trybot
rabbitQueue = "cicd"

patchwork_token = "fill in your patchwork api token here"

# other parameters specific to the job the trybot does

trybot_dir = os.environ['HOME'] + "glibc-cicd/tools/cicd/trybots/"
webbase = "/home/web/trybots/"
webbaseurl = "https://www.delorie.com/trybots/"

rebuild_script = os.environ['HOME'] + "glibc-cicd/tools/podman/rebuild-baseline"
rebuild_check = os.environ['HOME'] + "glibc-cicd/tools/podman/check.sum"

