#!/usr/bin/env python

import argparse
import requests
import os

#------------------------------------------------------------------

saved_patch_id = 0;
saved_context = '';
saved_description = '';
saved_url = '';

def check_setup (context='', description='', url=''):
    global saved_context
    global saved_description
    global saved_url

    if context != '':
        saved_context = context
    if description != '':
        saved_description = description
    if url != '':
        saved_url = url

def check (state):
    global saved_context
    global saved_description
    global saved_url
    global saved_patch_id

    #print ("CHECK:", saved_patch_id, state, saved_context, saved_description, saved_url, "\033[0m")
    print ("CHECK:", saved_patch_id, state, saved_context, "\033[0m")
    if saved_patch_id == 0:
        return
    q = {'state' : state};
    if saved_context != None:
        q['context'] = saved_context;
    if saved_description != None:
        q['description'] = saved_description;
    if saved_url != None:
        q['target_url'] = saved_url;
    qurl = patchworkURL + "/api/1.2/patches/" + str(saved_patch_id) + "/checks/"
    h = { "Authorization" : "Token " + patchwork_token }
    response = requests.post (qurl, data = q, headers = h)
    if response.status_code >= 300:
        print ("q =", q);
        print ("Response:", response.status_code)
        print (response.text)
        print ("")

def check_pending ():
    print ("\033[34m", end='')
    check ('pending')

def check_success ():
    print ("\033[32m", end='')
    check ('success')

def check_warning ():
    print ("\033[33m", end='')
    check ('warning')

def check_fail ():
    print ("\033[31m", end='')
    check ('fail')

def check_set_patch (p):
    global saved_patch_id
    saved_patch_id = p

#------------------------------------------------------------------

"""
Parse command line args. """
def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--patch_id")
    parser.add_argument("--context")
    parser.add_argument("--description")
    parser.add_argument("--url")
    parser.add_argument("--state")
    return parser.parse_args()

def main():
    args = parse_args()

    check_set_patch(args.patch_id)
    check_setup(context = args.context, description = args.description,
                url = args.url)
    check(args.state)

# find and read the config file.  We do this here so that it's
# available both when this file is imported, and when it's run from
# the command line.
mypath = os.path.realpath (__file__)
mydir = os.path.dirname (mypath)
myconfig = mydir + "/cicd-config.py"
exec (compile (filename=myconfig, source=open (myconfig).read (), mode="exec"))

if __name__ == "__main__":
    main()
