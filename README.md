# GLIBC CICD

CI/CD Sample scripts for GLIBC

I haven't written much documentation yet, but these are the scripts
I'm running...

Wiki: https://sourceware.org/glibc/wiki/CICDDesign

You must edit `cicd-config.py` to at least include your patchwork API
token and paths to working directories.  Also, edit `rebuild-baseline`
if you use that (it's in `trybots/32bit/`)

Some of the trybots assume a pre-checked-out git area, so caveat
emptor.

The runner looks for files named `runner_*.py` and executes them for
each incoming event - this is where YOU decide which events you're
interested in, and what those events trigger.

`runner-inject.py` lets you manually inject an event into your local
queue, for testing trybots or re-running jobs.

The curator and runner assume you have mariadb installed and running.

The runner and trybots assume you have rabbitmq installed and running.

Documentation for setting everything up is still in flux :-)

Note: `notes.txt` are my local notes and do not completely match
reality, but do have some notes on setting everything up since that's
where I record them.

## Installing

The various scripts (curator, runner, trybots) need not run on the
same host, but they may, even in the same directory.  One config file
can serve multiple scripts, and each script looks for its config file
in the same directory as the script itself.  This config file is
`cicd-config.py` so if all your scripts run from the same directory,
you'll have one big `cicd-config.py`, and if you run each script in
its own directory, you'll have many smaller `cicd-config.py`'s

There are no defaults if the config file is missing; a missing file or
missing entries will cause the scripts to fail.

The host that the curator runs on needs the following packages
installed:

	python3-gnupg
	python3-mysqlclient
	python3-pygit2

As well as installing the python mariadb connector:

	pip install mariadb

The host that the runner runs on needs the following packages
installed:

	python3-mysqlclient
	python3-pika - for RabbitMQ

As well as installing the python mariadb connector:

	pip install mariadb

The hosts that the trybots run on need the following packages
installed (in addition to whatever support packages the specific
trybots need, of course):

	python3-pika - for RabbitMQ

Note that the curator and runner need access to a MariaDB SQL server.
This server currently needs to be on the same host as the curator or
runner (because I'm lazy).  The curator and runner each access their
own databases (cicddb and runnerdb, respectively) initialized with the
`db-init.sql` script.  Suitable access for the user running the
curator/runner must be arranged by the maradb administrator.

Note that the runners and trybots communicate using a common rabbitmq
message bus.  The rabbitmq server need not run on the same host as
these scripts.  The rabbitmq server must be configured to allow the
runners and trybots to use it, although no queue configuration is
required.

## Configuring

Note that these configuration files are plain Python scripts, with the
usual flexibility and security issues.

### Curator

The curator reads `cicd-config.py`, in the same directory as the
curator script itself, for its configuration, and ignores any settings
it doesn't recognize.  The sample config file given includes
documentation (in the form of comments) about each supported config
variable.

The curator host must run about five copies of the curator, depending
on configuration:

	curator poll

This process polls the configured patchwork instance for new events.
When new events happen, they are added to the list of events in the
local database.

	curator git

This process periodically does a "git pull" on the configured git
repo, and if any new commits are noted, appropriate events are added
to the local database.  Note that you must pre-clone the git repo
specified in the config file.

	curator prune

This process deletes events in the local database that are older than
the configured time limit.

	curator http

This optional process acts like a web micro-server.  The URL is
ignored; only the since= cgi parameter is used.  The result is always
a JSON list of events; either all unexpired events or those happening
after the "since=" parameter.

	curator email

TBD - this handles emails to the mailing list that are GPG-signed with
known keys, acting as "commands" to various trybots.

	curator cgi

This is the only invokation that is not a non-exiting "daemon" like
the previous ones.  This option is used when you have an existing web
server and want to serve JSON events via a CGI script running on that
web server; you can run this command from within a CGI script, or via
a symlink into the cgi-bin directory (in which case, the 'cgi' command
line parameter is not required)

### Runner

The runner also reads `cicd-config.py`, in the same directory as the
runner script itself, for its configuration.

Each site that provides trybot services runs its own runner; the
purpose of the runner is to decide which events get passed to which
local trybots.

In addition to the configuration file, the runner will look in the
directory in which the runner script exists, searching for files named
`runner_*.py'.  Each file has a tiny bit of python code that looks in
the `event[]' dictionary and, based on the contents, calls
add_tryjob() zero or more times.  Each call adds a message in the
named rabbitmq queue, which the various trybots watch for.

### Trybots

These are the most customized of the scripts.  Rather than providing a
master trybot script, a trybot "class" is available.  Each trybot
imports the class and uses it to do common tasks.  The only required
call is:

	trybot.start ('queue-name', callback)

The callback you provide will be passed the `event[]` dictionary.

Trybots use the same config file as the other scripts; in addition to
the common config options, you must create a "patchwork api token" in
the patchwork instance you're referring to, and put that in the config
file as the `patchwork_token`.  To do this, log in to patchwork, view
your profile, and generate an API token in the "Authentication" box.

Trybots can report their success or failure using the various
trybot::check_*() functions.

	trybot.check_setup ('context', 'description', 'url')

Set up the context of the check the trybot does.  *Context* is a
keyword that uniquely identifies the check.  *Description* is a
user-readable description of the test, or if the test fails, a
failure-specific description, or, if you wish, a description of the
URL.  *Url* links to a web page with further failure information
(i.e. your trybot could store logs somewhere, and link to them).  Note
that Patchwork combines the description and url into a single
hyperlink, and prepends your username to the context, so you need not
do those steps yourself.

	trybot.check_set_patch (p)

Patchwork assigns checks to patches (note: not series).  Each trybot
must specify a patch id; the base class will extract the last patch of
the event's patch series (if the event is a patch series) as the
default patch to attach checks to; a trybot which does per-patch
checks uses this call to direct the check to the appropriate patch.

	trybot.check_pending ()
	trybot.check_warning ()
	trybot.check_fail ()
	trybot.check_success ()

These report the appropriate status back to patchwork.

#### Builder Trybots

The git repo includes two sample trybots; one that just tests if a
patch applies, and one that does a 32-bit build in a container.  The
apply-patch one creates its own subdir and initializes it, but the
builder one requires some manual setup.

You will be editing these sample trybots if you use them; rename them
to something specific to your check and read through them, editing as
needed.  For example, the 32-bit builder trybot has "`32bit/`" hardcoded
in a few places, plus the names of the containers and images it will
use.

To set up a new builder, you must set up the base container images it
will use.  Install podman.  In the `trybots/32bit/` subdirectory
you'll find templates for the base image and the trybot image, as well
as a script that rebuilds the base image.  Note that these have
hardcoded paths in them!  The rebuild script is run by the trybot, but
it will place needed output files (specifically, `check.sum`) in its
working directory, and the trybot needs to know where that is.

The baseline template includes a checked-out copy of the git repo, and
the rebuild script only does a "git pull" on it.  You must pre-clone
the git repo into the appropriate place (it shows up as `/glibc/` in
the container, see the `rebuild-baseline` script for details)

If your trybot assumes the baseline is clean (see 'baseline_is_dirty'
therein), you need to run `rebuild-baseline` once before starting the
trybot.

Note: if you're using rootless containers, you might need to run
`loginctl enable-linger 123` (or whatever pid you're using) so that the
systemd user session for the containers' user doesn't go away and
break all your containers.

## Running

Starting up the scripts is up to the administrator; I have a script
that just does this, which I run manually:

	#!/bin/bash
	
	cd $HOME/tools/cicd
	
	./curator.py prune >> logs/c-prune.log 2>&1 &
	./curator.py poll >> logs/c-poll.log 2>&1 &
	./curator.py git >> logs/c-git.log 2>&1 &
	./curator.py http >> logs/c-http.log 2>&1 &
	
	sleep 2
	
	./runner.py >> logs/runner.log 2>&1 &
	
	while true; do ./trybot-32bit.py >> logs/t-32bit.log 2>&1 </dev/null; done &
	while true; do ./trybot-apply_patch.py >> logs/t-apply_patch.log 2>&1 </dev/null ; done &

In the future, control should be through systemd.
